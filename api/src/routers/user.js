const express = require('express')
const User = require('../models/User')
const Role = require('../models/Role');
const EmailToken = require('../models/Token');
const auth = require('../middleware/auth');
const config = require('../config');
const ROLES = config.ROLES;
const crypto = require('crypto');
const { check, validationResult } = require('express-validator');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const router = express.Router()

let checkRoles = (req, res, next) => {
  for (let i = 0; i < req.body.roles.length; i++) {
    if (!ROLES.includes(req.body.roles[i].toUpperCase())) {
      res.status(400).send("Fail -> Does NOT exist Role = " + req.body.roles[i]);
      return;
    }
  }
  next();
}

router.post('/users', [
  checkRoles,
  check('firstname').isString(),
  check('lastname').isString(),
  check('email').isEmail(),
  check('password').isLength({ min: 8 })
], async (req, res) => {
  const errors = validationResult(req);
  const passwordsAreEquals = req.body.password === req.body.confirmPassword;
  if (!errors.isEmpty() || !passwordsAreEquals) {
    return res.status(422).json({ errors: errors.array() });
  }

  // Create a new user
  try {
    const user = new User(req.body)
    await user.save().then(async (savedUser) => {
      await Role.find({
        'name': { $in: req.body.roles.map(role => role.toUpperCase()) }
      }, (err, roles) => {
        if (err)
          res.status(500).send("Error -> " + err);

        // update User with Roles
        user.roles = roles.map(role => role.name);
      });

      // Create a verification token for this user
      var emailToken = new EmailToken({ _userId: savedUser._id, emailToken: crypto.randomBytes(16).toString('hex') });
      // Save the verification token
      await emailToken.save(async function (err) {
        if (err) { return res.status(500).send({ msg: err.message }); }

        // Send the email
        var mailOptions = {
          from: 'no-reply@velovolelille.org',
          to: user.email,
          subject: 'Vérification de compte velovolelille.org',
          text: `Hello,\n\n
         Merci de confirmer votre inscription en cliquant sur ce lien : \n
         ${process.env.APP_URL}/users/confirmation/${emailToken.emailToken}`
        };
        await sgMail.send(mailOptions).then(async () => {
          delete req.body.password;
          res.status(201).send(req.body);
        });
      });
    })
  } catch (error) {
    console.error;
    res.status(400).send(error)
  }
})

router.post('/users/resend', [check('email').isEmail()], (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }

  User.findOne({ email: req.body.email }, (err, user) => {
    if (!user) return res.status(400).send({ msg: 'We were unable to find a user with that email.' });
    if (user.isVerified) return res.status(400).send({ msg: 'This account has already been verified. Please log in.' });

    // Create a verification token, save it, and send email
    var emailToken = new EmailToken({ _userId: user._id, emailToken: crypto.randomBytes(16).toString('hex') });

    // Save the token
    emailToken.save((err) => {
      if (err) { return res.status(500).send({ msg: err.message }); }

      // Send the email
      var mailOptions = {
        from: 'no-reply@velovolelille.org',
        to: user.email,
        subject: 'Account Verification Token',
        text: `Hello,\n\n
         Merci de confirmer votre inscription en cliquant sur ce lien : \n
         ${process.env.APP_URL}/users/confirmation/${emailToken.emailToken}`
      };
      sgMail.send(mailOptions).then(() => {
        return res.send(201, req.body);
      });
    });

  });
})

router.get('/users/confirmation/:token', async (req, res) => {
  const validToken = req.params.token;

  // Find a matching token
  EmailToken.findOne({ emailToken: validToken }, function (err, token) {
    if (!token) return res.status(400).send({ type: 'not-verified', msg: 'We were unable to find a valid token. Your token my have expired.' });

    // If we found a token, find a matching user
    User.findOne({ _id: token._userId }, function (err, user) {
      if (!user) return res.status(400).send({ msg: 'We were unable to find a user for this token.' });
      if (user.isVerified) return res.status(400).send({ type: 'already-verified', msg: 'This user has already been verified.' });

      // Verify and save the user
      user.isVerified = true;
      user.save(function (err) {
        if (err) { return res.status(500).send({ msg: err.message }); }
        EmailToken.findOneAndDelete({emailToken: validToken}, (err) => {
          if (err) {
            return res.status(500).send({ msg: err.message });
          }
        });
        return res.status(200).send({ msg: "The account has been verified. Please log in." });
      });
    });
  });
})

router.post('/users/login', async (req, res) => {
  //Login a registered user
  try {
    const { email, password } = req.body
    const user = await User.findByCredentials(email, password)
    if (!user) {
      return res.status(401).send({ error: 'Login failed! Check authentication credentials' })
    }
    const token = await user.generateAuthToken()
    return res.send({ token })
  } catch (error) {
    return res.status(400).send(error)
  }

})

router.get('/users/me', auth.verifyToken, async (req, res) => {
  // View logged in user profile
  res.send(req.user)
})

router.post('/users/me/logout', auth.verifyToken, async (req, res) => {
  // Log user out of the application
  try {
    req.user.tokens = req.user.tokens.filter((token) => {
      return token.token != req.token
    })
    await req.user.save()
    res.send()
  } catch (error) {
    res.status(500).send(error)
  }
})

router.post('/users/me/logoutall', auth.verifyToken, async (req, res) => {
  // Log user out of all devices
  try {
    req.user.tokens.splice(0, req.user.tokens.length)
    await req.user.save()
    res.send()
  } catch (error) {
    res.status(500).send(error)
  }
})

router.post('/forgot', [check('email').isEmail()], (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }

  User.findOne({ email: req.body.email }, (err, user) => {
    if(!user) {
      return res.send(201, req.body);
    } else {
      // Create a verification token, save it, and send email
    var emailToken = new EmailToken({ _userId: user._id, emailToken: crypto.randomBytes(16).toString('hex') });
    // Save the token
    emailToken.save((err) => {
      if (err) { return res.status(500).send({ msg: err.message }); }

      // Send the email
      var mailOptions = {
        from: 'no-reply@velovolelille.org',
        to: user.email,
        subject: 'Changement de mot de passe',
        text: `Vous recevez cet email car vous (ou quelqu'un d'autre) a demandé la réinitialisation du mot de passe de votre compte.\n\n
                 Veuillez cliquer sur le lien suivant ou collez-le dans votre navigateur pour terminer le processus:\n
       ${process.env.APP_URL}/reset/${emailToken.emailToken}.\n
       Si vous ne l'avez pas demandé, veuillez ignorer cet e-mail et votre mot de passe restera inchangé.`
      };
      sgMail.send(mailOptions).then(() => {
        return res.send(201, req.body);
      });
    });
    };
  });
});

router.put('/reset/:token', [check('password').isLength({min: 8 })], async (req, res) => {
  const resetToken = req.params.token;
  // Find a matching token
  EmailToken.findOne({ emailToken: resetToken }, function (err, token) {
    if (!token) return res.status(400).send({ type: 'not-verified', msg: 'We were unable to find a valid token. Your token my have expired.' });

    // If we found a token, find a matching user
    User.findOne({ _id: token._userId }, function (err, user) {
      if (!user) return res.status(400).send({ msg: 'We were unable to find a user for this token.' });

      // Verify and save the user
      user.password = req.body.password;
      user.save(function (err) {
        if (err) { return res.status(500).send({ msg: err.message }); }
        EmailToken.findOneAndDelete({emailToken: resetToken}, (err) => {
          if (err) {
            return res.status(500).send({ msg: err.message });
          }
        });
        return res.status(200).send({ msg: "The password has been modified. Please log in." });
      });
    });
  });
})

module.exports = router
