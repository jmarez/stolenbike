const express = require('express');
const Message = require('../models/Message');
const User = require('../models/User');
const router = express.Router();
const { check, validationResult } = require('express-validator');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

router.post('/messages', [
    check('message').isString(),
    check('bikeId').isString(),
    check('bikeOwnerId').isString(),
    check('name').isString(),
    check('email').isEmail()
], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    };
    let message = new Message();
    message.bikeId = req.body.bikeId;
    message.bikeOwnerId = req.body.bikeOwnerId;
    message.message = req.body.message;
    message.name = req.body.name;
    message.email = req.body.email;
    message.save().then((data, err) => {
        if (err) {
            return res.status(500).send();
        }
        User.findOne({ _id: req.body.bikeOwnerId }, async (err, user) => {
            var mailOptions = {
                from: 'no-reply@velovolelille.org',
                to: user.email,
                subject: 'Des infos à propos de ton vélo volé',
                text: `Hello,
                \n${message.name} (${message.email}) a des infos sur ton vélo volé.
                \n${message.message}
                \n\nwww.velovolelille.org
                `
            };
            await sgMail.send(mailOptions).then(async () => {
                console.log('message sent !')
                res.status(201).send(data);
            });
        });
    });
});

module.exports = router;
