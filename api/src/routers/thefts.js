const express = require('express');
const Theft = require('../models/Theft')
const auth = require('../middleware/auth');
const router = express.Router();
const multer = require('multer');
const GridFsStorage = require('multer-gridfs-storage');
const Grid = require('gridfs-stream');
const mongoose = require('mongoose');

const conn = mongoose.connection;

// Init gfs
let gfs;

conn.once('open', () => {
    // Init stream
    gfs = Grid(conn.db, mongoose.mongo);
    gfs.collection(); //collection name
});

// Create a storage object with a given configuration
const storage = new GridFsStorage({ url: process.env.MONGODB_URL });

// Set multer storage engine to the newly created object
const upload = multer({
    storage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg" || file.fileSize <= 5000) {
            cb(null, true);
        } else {
            cb(null, false);
            return cb(new Error('Only .png, .jpg and .jpeg format and file size under 5Mb allowed!'));
        }
    }
});

router.post('/thefts', auth.verifyToken, upload.single('picture'), async (req, res) => {
    console.log(req.file)
    let theft = new Theft(JSON.parse(req.body.theft));
    theft.picture = req.file.filename;
    theft.authorId = req.user._id;
    theft.save().then((data, err) => {
        if (err) {
            return res.status(500).send();
        }
        return res.status(201).send(data);
    })
});

router.get('/thefts', async (req, res) => {
    Theft.find({}, (err, thefts) => {
        if (err) {
            return res.status(500).send(err)
        }
        return res.status(201).send(thefts);
    });
});

router.get('/thefts/images/:filename', (req, res) => {
    gfs.files.findOne({filename: req.params.filename}, (err, file) => {
        if(!file || file.length === 0){
            return res.status(404).json();
        } else {
            // Check if is image
            if(file.contentType === "image/jpeg" || file.contentType === "image/png"){
                // Read output to broswer

                const readstream = gfs.createReadStream({filename: file.filename, contentType: file.contentType});
                res.set('Content-Type', file.contentType);
                readstream.pipe(res);
            } else {
                res.status(404).json({err: 'Not and image'});
            }
        }
    });
});

router.get('/thefts/details/:id', (req, res) => {
    Theft.findOne({_id: req.params.id}, (err, detail) => {
        if (err) {
            return res.status(500).send(err)
        }
        res.status(200).json(detail);
    })
})

module.exports = router
