const mongoose = require('mongoose'), Schema = mongoose.Schema;

const tokenSchema = Schema({
  _userId: { type: Schema.Types.ObjectId, required: true, ref: 'User' },
  emailToken: { type: String, required: true },
  createdAt: { type: Date, required: true, default: Date.now, expires: 43200 }
});

const EmailToken = mongoose.model('EmailToken', tokenSchema)

module.exports = EmailToken
