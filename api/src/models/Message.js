const mongoose = require('mongoose'), Schema = mongoose.Schema;
const validator = require('validator')

const MessageSchema = Schema({
    message: {
        type: String,
        require: true
    },
    bikeOwnerId: {
        type: String,
        required: true
    },
    bikeId: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        lowercase: true,
        validate: value => {
            if (!validator.isEmail(value)) {
                throw new Error({ error: 'Invalid Email address' })
            }
        }
    }
});

module.exports = mongoose.model('Message', MessageSchema);
