const mongoose = require('mongoose'), Schema = mongoose.Schema;

const TheftSchema = Schema({
    bikeType: {
        type: String,
        required: true
    },
    brand: {
        type: String,
        required: true
    },
    color: {
        type: String,
        required: true
    },
    hasBicycode: {
        type: Boolean,
        required: true
    },
    bicycode: String,
    description: {
        type: String,
        required: true
    },
    date: {
        type: String,
        required: true
    },
    time: {
        type: String,
        required: true
    },
    picture: Object,
    address: {
        type: Object,
        required: true
    },
    fixedPoint: {
        type: Boolean,
        required: true
    },
    lockType: {
        type: String,
        required: true
    },
    authorId: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Theft', TheftSchema);
