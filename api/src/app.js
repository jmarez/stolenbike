const express = require('express');
const userRouter = require('./routers/user');
const theftRouter = require('./routers/thefts');
const messageRouter = require('./routers/message');
const cors = require('cors');
const port = process.env.PORT;
const bodyParser = require('body-parser');
const RateLimit = require('express-rate-limit');
const path = require('path');

const limiter = new RateLimit({
  windowMs: 1*60*1000, // 1 minute
  max: 600
});

var corsOptions = {
    origin: process.env.APP_URL,
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

require('./db/db')

const app = express();

if (process.env.PROD_ENABLED === 'true') {
    app.use(express.static(path.resolve(__dirname, '../../dist/biketheftsmap')));
    // exclude /api
    app.get(/^\/(?!api).*$/, (req, res) => {
        res.sendFile(path.resolve(__dirname, '../../dist/biketheftsmap/index.html'));
    })
}
// apply rate limiter to all requests
app.use(limiter);
app.use(express.json());
app.use(cors(corsOptions));
if(process.env.PROD_ENABLED === 'true') {
    app.use('/api', userRouter);
    app.use('/api', theftRouter);
    app.use('/api', messageRouter);

} else {
    app.use(userRouter);
    app.use(theftRouter);
    app.use(messageRouter);
}
app.use(bodyParser.json());

app.listen(port, () => {
  console.log(`API running on port ${port}`);
})
