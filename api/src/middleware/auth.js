const jwt = require('jsonwebtoken')
const User = require('../models/User')
const Role = require('../models/Role.js');

const verifyToken = async(req, res, next) => {
    const token = req.header('Authorization').replace('Bearer ', '')
    const data = jwt.verify(token, process.env.JWT_KEY)
    try {
        const user = await User.findOne({ _id: data._id, 'tokens.token': token })
        if (!user) {
            throw new Error()
        }
        req.user = user
        req.token = token
        next()
    } catch (error) {
        res.status(401).send({ error: 'Not authorized to access this resource' })
    }
}

const isAdmin = (req, res, next) => {
  User.findOne({ _id: req.user._id })
  .exec((err, user) => {
    if (err){
      if(err.kind === 'ObjectId') {
        return res.status(404).send({
          message: "User not found with Username = " + req.body.username
        });
      }
      return res.status(500).send({
        message: "Error retrieving User with Username = " + req.body.username
      });
    }
    Role.find({
      '_id': { $in: user.roles }
    }, (err, roles) => {
      if(err)
        res.status(500).send("Error -> " + err);

      for(let i=0; i<roles.length; i++){
        if(roles[i].name.toUpperCase() === "ADMIN"){
          next();
          return;
        }
      }

      res.status(403).send("Require Admin Role!");
      return;
    });
  });
}

let auth = {};
auth.verifyToken = verifyToken;
auth.isAdmin = isAdmin;

module.exports = auth
