## Pourquoi attacher son vélo ?

Actuellement, en ville comme un peu partout, le vélo connaît un énorme succès. Normal, il s’agit d’un moyen de locomotion aux multiples avantages (mais vous le savez déjà). Et en toute logique, les vols de vélo augmentent. L’an dernier par exemple, quelques 500.000 vélos ont été volés en France.

Avec un bon antivol, et en veillant à bien attacher son vélo, il est possible de réduire les risques de vol. 


## Utiliser un bon antivol

Equipez vous d’un antivol en U. Un mauvais U vaudra toujours mieux qu’un cable, mais tant qu’à utiliser un U, achetez en un de bonne qualité.

Vous pouvez regarder [les résultats des tests d’antivol de la FUB](https://www.bicycode.org/test-antivol/) pour faire votre choix ou demandez conseils auprès de la communauté [Cyclistes à Lille](https://www.facebook.com/groups/119800094697738/)

## Attacher son vélo à un point fixe

Le cadre en priorité et si possible la roue avant, surtout si celle ci est équipée d'attache rapide. Pensez à bien vérifier le point fixe sur le lequel on s’attache. Est il bien scellé / ancré dans le sol ? Dans le cas des attaches vélos que l’on retrouve en ville, les tubes n'ont ils pas été sciés pour simplifier le vol ?

![Arceau coupé à Lille](./assets/articles/images/arceau_cut.jpg)

📸 [@cyclogruff](https://twitter.com/CycloGruff/status/1168958664604037125?s=19)

### Quelques conseils en vidéo

<iframe class="youtube__iframe" width="560" height="315" src="https://www.youtube.com/embed/QW7L928yoiM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Toujours attacher son vélo


Dans son hall d'immeuble, sa cave, son garage et surtout dans la rue. Même pour un passage éclair dans un magasin, attachez votre vélo ou rentrez avec si le commerçant est sympa. 

## Éviter les attaches rapides. 

Même si ils sont bien utiles pour une pratique sportive, pour un usage velotaf quotidien, cela reste des points faibles pour un voleur opportuniste qui en profiterait pour vous voler votre selle ou votre roue. Pour éviter ça, vous pouvez vous équiper d'attache antivol ou de roue à serrage par écrou. 

Gardez en tête que même le meilleur antivol n'est qu'un retardateur pour un voleur. 
