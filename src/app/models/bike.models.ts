export interface BikeType {
    label: string;
    value: string;
}

export interface Place {
    label: string;
    value: string;
}

export interface LockType {
    label: string;
    value: string;
}
