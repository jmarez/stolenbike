export interface ITheft {
    _id?: string;
    bikeType: string;
    brand: string;
    color: string;
    hasBicycode: boolean;
    bicycode?: string;
    description: string;
    date: string;
    time: string;
    picture: any;
    address: any;
    fixedPoint: boolean;
    lockType: string;
    authorId?: string;
}
