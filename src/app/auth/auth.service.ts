import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from './user.model';
import { environment } from '../../environments/environment';
import * as jwtDecode from 'jwt-decode';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<User>(
            JSON.parse(localStorage.getItem('currentUser'))
        );
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(username: string, password: string) {
        return this.http
            .post<User>(environment.apiUrl + '/users/login', {
                email: username,
                password
            })
            .pipe(
                map(user => {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.currentUserSubject.next(user);
                    return user;
                })
            );
    }

    logout() {
        return this.http
            .post<any>(
                environment.apiUrl + '/users/me/logout',
                {},
                {
                    headers: {
                        Authorization: `Bearer ${this.currentUserValue.token}`
                    }
                }
            )
            .pipe(
                map(() => {
                    // remove user from local storage to log user out
                    localStorage.removeItem('currentUser');
                    this.currentUserSubject.next(null);
                })
            );
    }

    isAuthorized(allowedRoles: string[]): boolean {
        // check if the list of allowed roles is empty, if empty, authorize the user to access the page
        if (allowedRoles == null || allowedRoles.length === 0) {
            return true;
        }
        let token: string;
        let decodeToken;

        // get token from local storage or state management
        if (localStorage.getItem('currentUser')) {
            token = JSON.parse(localStorage.getItem('currentUser')).token;
            // decode token to read the payload details
            decodeToken = jwtDecode(token);
        } else {
            return false;
        }
        // check if it was decoded successfully, if not the token is not valid, deny access
        if (!decodeToken) {
            return false;
        }
        // check if the user roles is in the list of allowed roles, return true if allowed and false if not allowed
        return allowedRoles.includes(decodeToken.roles[0][0]);
    }

    confirm(token: string) {
        return this.http.get<string>(
            environment.apiUrl + '/users/confirmation/' + token
        );
    }

    forgotPassword(email: string) {
        return this.http.post<string>(environment.apiUrl + '/forgot', {
            email
        });
    }

    resetPassword(token: string, password: string) {
        return this.http.put(environment.apiUrl + '/reset/' + token, {
            password
        });
    }

    getTokenSilently() {
        if (this.currentUserValue) {
            return of(this.currentUserValue.token);
        } else {
            return of(null);
        }
    }
}
