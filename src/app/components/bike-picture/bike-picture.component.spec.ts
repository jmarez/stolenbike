import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BikePictureComponent } from './bike-picture.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('BikePictureComponent', () => {
  let component: BikePictureComponent;
  let fixture: ComponentFixture<BikePictureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BikePictureComponent ],
      imports: [HttpClientTestingModule]
    })
    .overrideTemplate(BikePictureComponent, '')
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BikePictureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
