import {
    Component,
    OnInit,
    Input,
    OnChanges,
    SimpleChanges,
    OnDestroy
} from '@angular/core';
import { ImageService } from 'src/app/services/image/image.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-bike-picture',
    templateUrl: './bike-picture.component.html',
    styleUrls: ['./bike-picture.component.scss']
})
export class BikePictureComponent implements OnInit, OnChanges, OnDestroy {
    @Input() bikePicture: string;
    imageToShow: any;
    isImageLoading: boolean;
    imageSubscribtion: Subscription;

    constructor(
        private imageService: ImageService,
        private sanitizer: DomSanitizer
    ) {}

    ngOnInit() {
        this.getImageFromService(this.bikePicture);
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.bikePicture) {
            this.getImageFromService(this.bikePicture);
        }
    }

    private getImageFromService(picture) {
        this.isImageLoading = true;
        this.imageToShow = null;

        this.imageSubscribtion = this.imageService.getImage(picture).subscribe(
            data => {
                this.createImageFromBlob(data);
                this.isImageLoading = false;
            },
            () => {
                this.imageToShow = 'error';
            }
        );
    }

    private createImageFromBlob(image: Blob) {
        const reader = new FileReader();
        reader.addEventListener(
            'load',
            () => {
                this.imageToShow = this.sanitizer.bypassSecurityTrustUrl(
                    reader.result.toString()
                );
            },
            false
        );

        if (image) {
            reader.readAsDataURL(image);
        }
    }

    ngOnDestroy() {
        if (this.imageSubscribtion) {
            this.imageSubscribtion.unsubscribe();
        }
    }
}
