import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../../auth/auth.service';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
    isLoggedIn: boolean;
    constructor(
        private authenticationService: AuthenticationService,
        private router: Router
    ) {}

    ngOnInit() {
        this.authenticationService.currentUser.subscribe(() => {
            if (this.authenticationService.currentUserValue) {
                this.isLoggedIn = true;
            } else {
                this.isLoggedIn = false;
            }
        });
    }

    logout() {
        this.authenticationService
            .logout()
            .pipe(first())
            .subscribe(() => {
                this.router.navigate(['']);
            });
    }
}
