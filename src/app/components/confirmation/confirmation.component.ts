import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/auth/auth.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-confirmation',
    templateUrl: './confirmation.component.html',
    styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {
    confirmedAccount: string;
    constructor(
        private auth: AuthenticationService,
        private route: ActivatedRoute
    ) {}

    ngOnInit() {
        this.auth.confirm(this.route.snapshot.params.token).subscribe(
            () => {
                this.confirmedAccount = 'success';
            },
            error => {
                this.confirmedAccount = error.error.type;
            }
        );
    }
}
