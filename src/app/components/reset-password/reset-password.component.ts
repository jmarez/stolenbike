import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'src/app/auth/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  resetPasswordFormgroup: FormGroup = new FormGroup({
    password: new FormControl('', [Validators.required, Validators.minLength(8)])
  });
  resetSuccess = false;
  resetError = false;

  constructor(private auth: AuthenticationService, private route: ActivatedRoute) { }

  ngOnInit() {
  }

  resetPassword() {
    const token: string = this.route.snapshot.params.token;
    this.auth.resetPassword(token, this.resetPasswordFormgroup.value.password)
      .subscribe(
        (data) => {
          this.resetSuccess = true;
          this.resetError = false;
        },
        (err) => {
          this.resetSuccess = false;
          this.resetError = true;
        }
      );
  }

}
