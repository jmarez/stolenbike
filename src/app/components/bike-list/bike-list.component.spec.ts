import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BikeListComponent } from './bike-list.component';
import { TheftsService } from 'src/app/services/thefts.service';
import { of, throwError } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('BikeListComponent', () => {
    let component: BikeListComponent;
    let fixture: ComponentFixture<BikeListComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [BikeListComponent],
            imports: [HttpClientTestingModule]
        })
            .overrideTemplate(BikeListComponent, '')
            .compileComponents();

        fixture = TestBed.createComponent(BikeListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should throw error', () => {
        const spyTheftsError = jest
            .spyOn(TheftsService.prototype, 'getThefts')
            .mockReturnValue(throwError(''));
        component.ngOnInit();
        expect(spyTheftsError).toHaveBeenCalled();
        expect(component.bikes).toBeFalsy();
    });

    it('should get thefts on init', () => {
        const thefts = [
            {
                bikeType: 'gravel',
                brand: 'fuji',
                color: 'bleu',
                hasBicycode: false,
                bicycode: '',
                description: 'hshsh lskdfjlqs lkqssfqsdklfn',
                date: '2019-12-16T23:00:00.000Z',
                time: '20:34',
                picture: 'FOO',
                address: {
                    x: '3.099643',
                    y: '50.6357533',
                    label:
                        `152, Rue du Becquerel, Fives, Lille,
                        Mons-en-Barœul, Lille, Nord, Hauts-de-France,
                        Metropolitan France, 59370, France`,
                    bounds: [
                        [50.6357033, 3.099593],
                        [50.6358033, 3.099693]
                    ],
                    raw: {
                        place_id: 30661811,
                        licence:
                            'Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright',
                        osm_type: 'node',
                        osm_id: 2746146824,
                        boundingbox: [
                            '50.6357033',
                            '50.6358033',
                            '3.099593',
                            '3.099693'
                        ],
                        lat: '50.6357533',
                        lon: '3.099643',
                        display_name:
                            '152, Rue du Becquerel, Fives, Lille, Mons-en-Barœul, Lille',
                        class: 'place',
                        type: 'house',
                        importance: 0.411
                    }
                },
                fixedPoint: false,
                lockType: 'ulock'
            }
        ];
        const spyThefts = jest
            .spyOn(TheftsService.prototype, 'getThefts')
            .mockReturnValue(of(thefts));
        component.ngOnInit();
        expect(spyThefts).toHaveBeenCalled();
        expect(component.bikes).toEqual(thefts);
    });
});
