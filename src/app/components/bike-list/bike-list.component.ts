import { Component, OnInit, OnDestroy } from '@angular/core';
import { TheftsService } from 'src/app/services/thefts.service';
import { ITheft } from 'src/app/models/theft.models';
import { DomSanitizer } from '@angular/platform-browser';
import { ImageService } from 'src/app/services/image/image.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-bike-list',
    templateUrl: './bike-list.component.html',
    styleUrls: ['./bike-list.component.scss']
})
export class BikeListComponent implements OnInit, OnDestroy {
    bikes: ITheft[];
    error: any;
    isImageLoading: boolean;
    imageToShow: any;
    theftsSubscribtion: Subscription;

    constructor(private theftsService: TheftsService, private sanitizer: DomSanitizer, private imageService: ImageService) {}

    ngOnInit() {
        this.theftsSubscribtion = this.theftsService.getThefts().subscribe(
            bikes => {
                bikes.sort((a, b) => new Date(b.date).getTime() - new Date(a.date).getTime());
                this.bikes = bikes;
            },
            error => {
                this.error = error;
            }
        );
    }

    ngOnDestroy() {
        if (this.theftsSubscribtion) {
            this.theftsSubscribtion.unsubscribe();
        }
    }
}
