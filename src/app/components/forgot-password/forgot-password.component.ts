import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/auth/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  forgotPasswordFormgroup: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email])
  });
  mailSentSuccess = false;
  mailSentError = false;

  constructor(private authService: AuthenticationService) { }

  ngOnInit() {
  }

  forgotPassword() {
    this.authService.forgotPassword(this.forgotPasswordFormgroup.value.email)
      .subscribe(
        () => {
          this.mailSentSuccess = true;
          this.mailSentError = false;
          this.forgotPasswordFormgroup.reset();
        },
        () => {
          this.mailSentSuccess = false;
          this.mailSentError = true;
        }
      );
  }

}
