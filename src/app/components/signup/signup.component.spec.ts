import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupComponent } from './signup.component';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { SignupService } from './signup.service';
import { throwError, of } from 'rxjs';

describe('SignupComponent', () => {
    let component: SignupComponent;
    let fixture: ComponentFixture<SignupComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [HttpClient, HttpHandler],
            declarations: [SignupComponent]
        })
            .overrideTemplate(SignupComponent, '')
            .compileComponents();

        fixture = TestBed.createComponent(SignupComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should submit form and return error', () => {
        const spyUser = jest
            .spyOn(SignupService.prototype, 'createAccount')
            .mockReturnValue(throwError('error'));
        component.onSubmit();
        expect(spyUser).toHaveBeenCalled();
        expect(component.signupSuccess).toBeFalsy();
        expect(component.signupError).toBeTruthy();
    });

    it('should submit form and return 200', () => {
        const spyUser = jest
            .spyOn(SignupService.prototype, 'createAccount')
            .mockReturnValue(
                of({
                    firstname: 'john',
                    lastname: 'doe',
                    email: 'email@email.com',
                    password: 'abcdefghij',
                    confirmPassword: 'abcdefghi',
                    roles: ['USER']
                })
            );

        component.onSubmit();
        expect(spyUser).toHaveBeenCalled();
        expect(component.signupError).toBeFalsy();
    });
});
