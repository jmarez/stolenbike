import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

export interface INewUser {
    firstname: string;
    lastname: string;
    email: string;
    password: string;
    confirmPassword: string;
    roles: string[];
}

@Injectable({
    providedIn: 'root'
})
export class SignupService {
    constructor(private http: HttpClient) {}

    createAccount(newUser: INewUser): Observable<INewUser> {
        return this.http.post<INewUser>(
            environment.apiUrl + '/users',
            newUser,
            { observe: 'body' }
        );
    }
}
