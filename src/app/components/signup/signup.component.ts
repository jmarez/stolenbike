import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SignupService } from './signup.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;
  signupSuccess = false;
  signupError = false;

  constructor(private signupService: SignupService) { }

  ngOnInit() {
    this.signupForm = new FormGroup({
      firstname: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.email, Validators.required]),
      password: new FormControl('', [Validators.minLength(7), Validators.required]),
      confirmPassword: new FormControl('', [Validators.minLength(7), Validators.required])
    });
  }

  onSubmit() {
    this.signupForm.value.roles = ['USER'];
    this.signupService.createAccount(this.signupForm.value).subscribe(
      () => {
        this.signupSuccess = true;
        this.signupError = false;
        this.signupForm.reset();
      },
      () => {
        this.signupSuccess = false;
        this.signupError = true;
      }
    );
  }

}
