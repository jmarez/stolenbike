import { TestBed } from '@angular/core/testing';

import { SignupService } from './signup.service';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('SignupService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      HttpClient,
      HttpHandler
    ]
  }));

  it('should be created', () => {
    const service: SignupService = TestBed.get(SignupService);
    expect(service).toBeTruthy();
  });
});
