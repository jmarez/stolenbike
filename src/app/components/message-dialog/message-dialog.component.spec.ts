import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { MessageDialogComponent } from './message-dialog.component';
import {
    NbDialogRef,
    NbToastrModule,
    NbDialogModule,
    NbOverlayModule,
    NbToastrService
} from '@nebular/theme';
import { AppModule } from 'src/app/app.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MessagesService } from 'src/app/services/messages/messages.service';
import { of } from 'rxjs';

describe('MessageDialogComponent', () => {
    let component: MessageDialogComponent;
    let fixture: ComponentFixture<MessageDialogComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [MessageDialogComponent],
            imports: [NbDialogModule, HttpClientTestingModule],
            providers: [
                {
                    provide: NbDialogRef,
                    useValue: {
                        close: jest.fn()
                    }
                },
                {
                    provide: NbToastrService
                }
            ]
        })
            .overrideTemplate(MessageDialogComponent, '')
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(MessageDialogComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
        component.ngOnInit();
        expect(component.messageFormGroup.invalid).toBeTruthy();
    });

    it('should cancel dialog and reset formgroup', () => {
        component.ngOnInit();
        component.messageFormGroup.setValue({
            name: 'john doe',
            email: 'johndoe@doo.com',
            message: 'my message'
        });
        expect(component.messageFormGroup.invalid).toBeFalsy();

        component.cancel();
        expect(component.messageFormGroup.invalid).toBeTruthy();
    });

    it('should submit dialog and send message', () => {
        component.ngOnInit();
        component.messageFormGroup.setValue({
            name: 'john doe',
            email: 'johndoe@doo.com',
            message: 'my message'
        });
        expect(component.messageFormGroup.invalid).toBeFalsy();

        const spyMessageService = jest
            .spyOn(MessagesService.prototype, 'sendMessage')
            .mockReturnValue(
                of({
                    name: 'john doe',
                    email: 'johndoe@doo.com',
                    message: 'my message',
                    bikeId: '1234',
                    bikeOwnerId: '3456'
                })
            );
        component.submit();
        expect(spyMessageService).toHaveBeenCalled();
    });
});
