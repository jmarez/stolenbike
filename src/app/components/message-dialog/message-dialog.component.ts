
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NbDialogRef, NbToastrService } from '@nebular/theme';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MessagesService } from 'src/app/services/messages/messages.service';
import { IMessage } from 'src/app/models/message.models';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-message-dialog',
    templateUrl: './message-dialog.component.html',
    styleUrls: ['./message-dialog.component.scss']
})
export class MessageDialogComponent implements OnInit, OnDestroy {
    @Input() bikeOwnerId: string;
    @Input() bikeId: string;

    messageFormGroup: FormGroup;
    messageSubscription: Subscription;
    constructor(
        protected dialogRef: NbDialogRef<MessageDialogComponent>,
        private toastrService: NbToastrService,
        private messagesService: MessagesService
    ) {}

    ngOnInit() {
        this.messageFormGroup = new FormGroup({
            name: new FormControl('', Validators.required),
            email: new FormControl('', [Validators.required, Validators.email]),
            message: new FormControl('', Validators.required)
        });
    }

    cancel() {
        this.messageFormGroup.reset();
        this.dialogRef.close();
    }

    submit() {
        const message: IMessage = this.messageFormGroup.value;
        message.bikeOwnerId = this.bikeOwnerId;
        message.bikeId = this.bikeId;
        this.messageSubscription = this.messagesService.sendMessage(this.messageFormGroup.value).subscribe(
            () =>
                this.toastrService.success(
                    'Votre message est envoyé.',
                    'Merci !'
                ),
            () =>
                this.toastrService.danger(
                    'Nous n’avons pas réussi à envoyer ce message.',
                    'Whoopsy...'
                )
        );
        this.dialogRef.close();
    }

    ngOnDestroy() {
        if (this.messageSubscription) {
            this.messageSubscription.unsubscribe();
        }
    }
}
