import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TheftDetailsComponent } from './theft-details.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { TheftsService } from 'src/app/services/thefts.service';
import { NbDialogModule, NbDialogService } from '@nebular/theme';

describe('TheftDetailsComponent', () => {
    let component: TheftDetailsComponent;
    let fixture: ComponentFixture<TheftDetailsComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [TheftDetailsComponent],
            imports: [HttpClientTestingModule, NbDialogModule],
            providers: [
                {
                    provide: NbDialogService
                },
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: { params: { id: '12345' } }
                    }
                }
            ]
        })
            .overrideTemplate(TheftDetailsComponent, '')
            .compileComponents();

        fixture = TestBed.createComponent(TheftDetailsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should get theft', () => {
        const bikeFormValue = {
            bikeType: 'gravel',
            brand: 'fuji',
            color: 'bleu',
            hasBicycode: false,
            bicycode: '',
            description: 'hshsh lskdfjlqs lkqssfqsdklfn',
            date: '2019-12-16T23:00:00.000Z',
            time: '20:34',
            picture: 'FOO',
            address: {
                x: '3.099643',
                y: '50.6357533',
                label:
                    '152, Rue du Becquerel, Fives, Lille, Mons-en-Barœul, Lille, Nord, Hauts-de-France, Metropolitan France, 59370, France',
                bounds: [
                    [50.6357033, 3.099593],
                    [50.6358033, 3.099693]
                ],
                raw: {
                    place_id: 30661811,
                    licence:
                        'Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright',
                    osm_type: 'node',
                    osm_id: 2746146824,
                    boundingbox: [
                        '50.6357033',
                        '50.6358033',
                        '3.099593',
                        '3.099693'
                    ],
                    lat: '50.6357533',
                    lon: '3.099643',
                    display_name:
                        '152, Rue du Becquerel, Fives, Lille, Mons-en-Barœul, Lille',
                    class: 'place',
                    type: 'house',
                    importance: 0.411
                }
            },
            fixedPoint: false,
            lockType: 'ulock'
        };
        expect(component.theft).toBeFalsy();
        const spyGetTheft = jest.spyOn(TheftsService.prototype, 'getTheftDetails').mockReturnValue(of(bikeFormValue));
        component.ngOnInit();
        expect(spyGetTheft).toHaveBeenCalled();
        expect(component.theft).toEqual(bikeFormValue);
        expect(component.error).toBeFalsy();
    });
});
