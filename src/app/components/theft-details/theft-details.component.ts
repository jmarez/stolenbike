import { Component, OnInit} from '@angular/core';
import { TheftsService } from 'src/app/services/thefts.service';
import { ActivatedRoute } from '@angular/router';
import { ITheft } from 'src/app/models/theft.models';
import * as L from 'leaflet';
import { NbDialogService } from '@nebular/theme';
import { MessageDialogComponent } from '../message-dialog/message-dialog.component';

@Component({
    selector: 'app-theft-details',
    templateUrl: './theft-details.component.html',
    styleUrls: ['./theft-details.component.scss']
})
export class TheftDetailsComponent implements OnInit {
    private map;
    theft: ITheft;
    error: any;
    imageToShow: any;
    isImageLoading: boolean;
    myIcon: L.Icon<L.IconOptions>;
    isLoading: boolean;
    theftId: any;

    constructor(
        private theftService: TheftsService,
        private route: ActivatedRoute,
        private dialogService: NbDialogService
    ) {
        this.theftId = this.route.snapshot.params.id;
    }

    ngOnInit() {
        this.theftService
            .getTheftDetails(this.theftId)
            .subscribe(
                theft => {
                    this.theft = theft;
                    this.initMap();
                    this.error = null;
                },
                error => {
                    this.error = error;
                }
            );
    }

    private initMap(): void {
        this.myIcon = L.icon({
            iconUrl: 'assets/marker.png',
            iconSize: [42, 45],
            shadowUrl: ''
        });
        this.map = L.map('theftMap', {
            center: [this.theft.address.y, this.theft.address.x],
            zoom: 17
        });
        const tiles = L.tileLayer(
            'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            {
                maxZoom: 19,
                attribution:
                    '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
            }
        );

        tiles.addTo(this.map);
        L.marker([
            this.theft.address.y,
            this.theft.address.x
        ], {icon: this.myIcon}).addTo(this.map);
    }

    openMessage() {
        this.dialogService.open(MessageDialogComponent, {context: {bikeId: this.theft._id, bikeOwnerId: this.theft.authorId}});
    }
}
