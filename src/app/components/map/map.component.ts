import {
    Component,
    OnInit,
    ComponentFactoryResolver,
    Injector,
    OnDestroy
} from '@angular/core';
import * as L from 'leaflet';
import { ICoordinates } from './map.models';
import { CoordinatesService } from 'src/app/services/coordinates.service';
import { CustomPopupComponent } from '../custom-popup/custom-popup.component';
import { TheftsService } from 'src/app/services/thefts.service';
import { ITheft } from 'src/app/models/theft.models';
import { DomSanitizer } from '@angular/platform-browser';
import { ImageService } from 'src/app/services/image/image.service';
import {
    GeoSearchControl,
    BingProvider
} from 'leaflet-geosearch';
import { Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, OnDestroy {
    private map;
    private coordinates: ICoordinates;
    isLoading: boolean;
    isImageLoading: boolean;
    imageToShow: any;
    pictureTheft: any;
    myIcon: L.Icon<L.IconOptions>;
    provider = new BingProvider({
        params: {
            key: environment.bingSecretKey
        }
    });
    searchControl = new GeoSearchControl({
        provider: this.provider,
        style: 'bar'
    });
    coordSub: Subscription;
    theftSub: Subscription;
    imageSub: Subscription;

    constructor(
        private coordinatesService: CoordinatesService,
        private componentFactoryResolver: ComponentFactoryResolver,
        private injector: Injector,
        private theftsService: TheftsService,
        private imageService: ImageService,
        private sanitizer: DomSanitizer
    ) {}

    ngOnInit() {
        this.isLoading = true;
        this.coordSub = this.coordinatesService.getPosition().subscribe(
            position => {
                this.getCoordinates(position);
            },
            error => {
                // init coordinates : Lille, FRANCE
                this.coordinates = {
                    latitude: 50.63297,
                    longitude: 3.05858
                };

                this.initMap();
            },
            () => {
                this.initMap();
            }
        );
    }

    private initMap(): void {
        this.myIcon = L.icon({
            iconUrl: 'assets/marker.png',
            iconSize: [42, 45],
            shadowUrl: ''
        });
        this.map = L.map('map', {
            center: [this.coordinates.latitude, this.coordinates.longitude],
            zoom: 15
        });
        this.isLoading = false;

        this.map.addControl(this.searchControl);

        const tiles = L.tileLayer(
            'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            {
                maxZoom: 19,
                attribution:
                    '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
            }
        );

        tiles.addTo(this.map);

        this.theftSub = this.theftsService
            .getThefts()
            .subscribe((thefts: ITheft[]) => {
                if (thefts) {
                    thefts.forEach((theft: ITheft) => {
                        const marker = L.marker(
                            [theft.address.y, theft.address.x],
                            { icon: this.myIcon }
                        ).addTo(this.map);
                        marker.bindPopup(() => this.createCustomPopup(theft));
                    });
                }
            });
    }

    private getCoordinates(position) {
        this.coordinates = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
        };
    }

    private createCustomPopup(theft: ITheft) {
        const factory = this.componentFactoryResolver.resolveComponentFactory(
            CustomPopupComponent
        );
        const component = factory.create(this.injector);

        // Set the component inputs manually
        component.instance.theft = theft;
        this.getImageFromService(theft.picture, component);

        // Manually invoke change detection, automatic wont work, but this is Ok if the component doesn't change
        component.changeDetectorRef.detectChanges();

        return component.location.nativeElement;
    }

    getImageFromService(picture, component) {
        this.isImageLoading = true;
        this.imageSub = this.imageService.getImage(picture).subscribe(
            data => {
                this.createImageFromBlob(data, component);
                this.isImageLoading = false;
            },
            error => {
                component.instance.imageToShow = 'error';
                component.changeDetectorRef.detectChanges();
            }
        );
    }

    createImageFromBlob(image: Blob, component) {
        const reader = new FileReader();
        reader.addEventListener(
            'load',
            () => {
                component.instance.imageToShow = this.sanitizer.bypassSecurityTrustUrl(
                    reader.result.toString()
                );
                component.changeDetectorRef.detectChanges();
            },
            false
        );

        if (image) {
            reader.readAsDataURL(image);
        }
    }

    ngOnDestroy() {
        if (this.theftSub) {
            this.theftSub.unsubscribe();
        }
        if (this.imageSub) {
            this.imageSub.unsubscribe();
        }
        if (this.coordSub) {
            this.coordSub.unsubscribe();
        }
    }
}
