import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapComponent } from './map.component';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { CoordinatesService } from 'src/app/services/coordinates.service';

describe('MapComponent', () => {
    let component: MapComponent;
    let fixture: ComponentFixture<MapComponent>;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [MapComponent],
            imports: [HttpClientTestingModule]
        })
            .overrideTemplate(MapComponent, '')
            .compileComponents();
        fixture = TestBed.createComponent(MapComponent);
        component = fixture.componentInstance;
        httpMock = TestBed.get(HttpTestingController);
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should init', () => {
        const spyCoord = jest.spyOn(CoordinatesService.prototype, 'getPosition').mockReturnValue(of(null));
        component.ngOnInit();
        expect(component.isLoading).toBeTruthy();
        expect(spyCoord).toHaveBeenCalledTimes(1);
    });
});
