import { Component, Input } from '@angular/core';
import { ITheft } from 'src/app/models/theft.models';

@Component({
    selector: 'app-custom-popup',
    templateUrl: './custom-popup.component.html',
    styleUrls: ['./custom-popup.component.scss']
})
export class CustomPopupComponent {
    @Input() theft: ITheft;
    @Input() imageToShow: any;
}
