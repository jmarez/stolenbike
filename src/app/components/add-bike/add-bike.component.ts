import {
    Component,
    OnInit,
    ViewChild,
    Output,
    EventEmitter,
    OnDestroy
} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AddressService } from 'src/app/services/address.service';
import {
    distinctUntilChanged,
    tap,
    switchMap,
    catchError
} from 'rxjs/operators';
import { Subject, of, concat, Observable, Subscription } from 'rxjs';
import { NgSelectComponent } from '@ng-select/ng-select';
import { TheftsService } from 'src/app/services/thefts.service';
import { BikeType, Place, LockType } from 'src/app/models/bike.models';
import { NbToastrService } from '@nebular/theme';

@Component({
    selector: 'app-add-bike',
    templateUrl: './add-bike.component.html',
    styleUrls: ['./add-bike.component.scss']
})
export class AddBikeComponent implements OnInit, OnDestroy {
    @ViewChild('selectAddresses', { static: true })
    selectAddresses: NgSelectComponent;
    @Output()
    uploadStatus = new EventEmitter();
    fileExt = 'JPG, PNG, JPEG';
    maxSize = 5; // 5MB
    inputAddress = new Subject<string>();
    addressLoading: boolean;
    bikeForm: FormGroup;
    bikeTypes: BikeType[] = [
        { value: 'vtt', label: 'VTT' },
        { value: 'vtc', label: 'VTC' },
        { value: 'gravel', label: 'Gravel' },
        { value: 'ville', label: 'Ville' },
        { value: 'route', label: 'Route' },
        { value: 'enfant', label: 'Enfant' },
        { value: 'pliant', label: 'Pliant' },
        { value: 'fixie', label: 'Fixie' },
        { value: 'couche', label: 'Couché' },
        { value: 'bmx', label: 'BMX' },
        { value: 'cyclocross', label: 'Cyclocross' },
        { value: 'tandem', label: 'Tandem' },
        { value: 'cargo', label: 'Cargo' }
    ];
    places: Place[] = [
        { value: 'private', label: 'Espace privé' },
        { value: 'common', label: 'Espace commun' },
        { value: 'public', label: 'Espace publique' }
    ];
    lockTypes: LockType[] = [
        { value: 'noop', label: 'Aucun' },
        { value: 'ulock', label: 'Cadenas en U' },
        { value: 'chain', label: 'Chaine' },
        { value: 'cable', label: 'Spirale / Câble' },
        { value: 'wheelLock', label: 'Roue arrière' }
    ];
    addresses$: Observable<any>;
    isSuccess: boolean;
    isError: boolean;
    errors: any[];
    bikePicture: any;
    isDisabled: boolean;
    theftSub: Subscription;

    constructor(
        private addressService: AddressService,
        private theftsService: TheftsService,
        private toastrService: NbToastrService
    ) {}

    ngOnInit() {
        this.bikeForm = new FormGroup({
            bikeType: new FormControl('', Validators.required),
            brand: new FormControl('', Validators.required),
            color: new FormControl('', Validators.required),
            hasBicycode: new FormControl(false),
            bicycode: new FormControl(''),
            description: new FormControl('', Validators.required),
            date: new FormControl('', Validators.required),
            time: new FormControl('', Validators.required),
            picture: new FormControl('', Validators.required),
            address: new FormControl('', Validators.required),
            fixedPoint: new FormControl(false),
            lockType: new FormControl('', Validators.required)
        });

        this.bikeForm.get('hasBicycode').valueChanges.subscribe(value => {
            if (value) {
                this.bikeForm
                    .get('bicycode')
                    .setValidators([
                        Validators.minLength(12),
                        Validators.pattern('^[0-9]*'),
                        Validators.maxLength(14),
                        Validators.required
                    ]);
            } else {
                this.bikeForm.get('bicycode').clearValidators();
            }
        });

        this.loadAddresses();
    }

    private loadAddresses() {
        this.addresses$ = concat(
            of([]), // default items
            this.inputAddress.pipe(
                distinctUntilChanged(),
                tap(() => (this.addressLoading = true)),
                switchMap(term => {
                    if (term) {
                        return this.addressService.getAddress(term).pipe(
                            catchError(() => of([])), // empty list on error
                            tap(() => {
                                this.addressLoading = false;
                                this.selectAddresses.isOpen = true;
                            })
                        );
                    } else {
                        this.addressLoading = false;
                        this.selectAddresses.isOpen = false;
                        return of([]);
                    }
                })
            )
        );
    }

    public submitBike() {
        const formData = new FormData();
        formData.append('theft', JSON.stringify(this.bikeForm.value));
        formData.append('picture', this.bikePicture, this.bikePicture.filename);
        this.isDisabled = true;
        this.theftSub = this.theftsService.addTheft(formData).subscribe(
            () => {
                this.toastrService.success('Votre vélo vient d’être ajouté.', 'Vélo ajouté !');
                this.bikeForm.reset();
            },
            error => {
                this.toastrService.danger('Il y a eu une erreur.', 'Whoopsy !');
            },
            () => {
                this.isDisabled = false;
            }
        );
    }

    public onFileChange(event) {
        this.errors = [];
        const file = event.target.files;
        if (file.length > 0 && !this.isValidFiles(file)) {
            this.uploadStatus.emit(false);
            return;
        }
        if (file.length > 0 && file.length === 1) {
            this.bikePicture = file[0];
        }
    }

    private isValidFiles(file) {
        // Check Number of files
        if (FileList.length > 0) {
            return false;
        }
        this.isValidFileExtension(file);
        return true;
    }

    private isValidFileExtension(file) {
        // Make array of file extensions
        const extensions = this.fileExt.split(',').map((x) => {
            return x.toLocaleUpperCase().trim();
        });
        // Get file extension
        const ext =
            file[0].name
                .toUpperCase()
                .split('.')
                .pop() || file[0].name;
        // Check the extension exists
        const exists = extensions.includes(ext);
        if (!exists) {
            this.errors.push('Error (Extension): ' + file[0].name);
        }
        // Check file size
        this.isValidFileSize(file[0]);
    }

    private isValidFileSize(file) {
        const fileSizeinMB = file.size / (1024 * 1000);
        const size = Math.round(fileSizeinMB * 100) / 100; // convert upto 2 decimal place
        if (size > this.maxSize) {
            this.errors.push(
                'Error (File Size): ' +
                    file.name +
                    ': exceed file size limit of ' +
                    this.maxSize +
                    'MB ( ' +
                    size +
                    'MB )'
            );
        }
    }

    ngOnDestroy() {
        if (this.theftSub) {
            this.theftSub.unsubscribe();
        }
    }
}
