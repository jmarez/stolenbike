import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBikeComponent } from './add-bike.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TheftsService } from 'src/app/services/thefts.service';
import { of } from 'rxjs';
import { NbToastrService } from '@nebular/theme';

describe('AddBikeComponent', () => {
    let component: AddBikeComponent;
    let fixture: ComponentFixture<AddBikeComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            declarations: [AddBikeComponent],
            providers: [
                {
                    provide: NbToastrService
                }
            ]
        })
            .overrideTemplate(AddBikeComponent, '')
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AddBikeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should init a formgroup', () => {
        component.ngOnInit();
        expect(component.bikeForm).toBeTruthy();
    });

    it('should set validators if bicycode', () => {
        component.ngOnInit();
        component.bikeForm.get('hasBicycode').setValue(true);
        expect(component.bikeForm.get('bicycode').validator).toBeTruthy();
        component.bikeForm.get('hasBicycode').setValue(false);
        expect(component.bikeForm.get('bicycode').validator).toBeFalsy();
    });

    it('should submit form if valid', () => {
        const bikeFormValue = {
            bikeType: 'gravel',
            brand: 'fuji',
            color: 'bleu',
            hasBicycode: false,
            bicycode: '',
            description: 'hshsh lskdfjlqs lkqssfqsdklfn',
            date: '2019-12-16T23:00:00.000Z',
            time: '20:34',
            picture: 'FOO',
            address: {
                x: '3.099643',
                y: '50.6357533',
                label:
                    '152, Rue du Becquerel, Fives, Lille, Mons-en-Barœul, Lille, Nord, Hauts-de-France, Metropolitan France, 59370, France',
                bounds: [
                    [50.6357033, 3.099593],
                    [50.6358033, 3.099693]
                ],
                raw: {
                    place_id: 30661811,
                    licence:
                        'Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright',
                    osm_type: 'node',
                    osm_id: 2746146824,
                    boundingbox: [
                        '50.6357033',
                        '50.6358033',
                        '3.099593',
                        '3.099693'
                    ],
                    lat: '50.6357533',
                    lon: '3.099643',
                    display_name:
                        '152, Rue du Becquerel, Fives, Lille, Mons-en-Barœul, Lille',
                    class: 'place',
                    type: 'house',
                    importance: 0.411
                }
            },
            fixedPoint: false,
            lockType: 'ulock'
        };
        component.bikePicture = new Blob();
        component.ngOnInit();
        expect(component.bikeForm.valid).toBeFalsy();
        component.bikeForm.setValue(bikeFormValue);
        expect(component.bikeForm.valid).toBeTruthy();
        const spySubmitTheft = jest
            .spyOn(TheftsService.prototype, 'addTheft')
            .mockReturnValue(of({}));
        component.submitBike();
        expect(spySubmitTheft).toHaveBeenCalledTimes(1);
    });
});
