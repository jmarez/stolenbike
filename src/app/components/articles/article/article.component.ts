import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-article',
    templateUrl: './article.component.html',
    styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
    articlePath: string;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.articlePath = this.activatedRoute.snapshot.data.articlePath;
    }
}
