import { NgModule, Injectable } from '@angular/core';
import {
    Routes,
    RouterModule,
    Resolve,
    ActivatedRouteSnapshot,
    RouterStateSnapshot
} from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './auth/auth.guard';
import { ProfileComponent } from './components/profile/profile.component';
import { HomeComponent } from './components/home/home.component';
import { SignupComponent } from './components/signup/signup.component';
import { ConfirmationComponent } from './components/confirmation/confirmation.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { AddBikeComponent } from './components/add-bike/add-bike.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { InterceptorService } from './services/interceptor/interceptor.service';
import { TheftDetailsComponent } from './components/theft-details/theft-details.component';
import { BikeListComponent } from './components/bike-list/bike-list.component';
import { ITheft } from './models/theft.models';
import { TheftsService } from './services/thefts.service';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { ArticleComponent } from './components/articles/article/article.component';


const routes: Routes = [
    { path: '', component: HomeComponent },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path: 'signup',
        component: SignupComponent
    },
    {
        path: 'confirmation/:token',
        component: ConfirmationComponent
    },
    {
        path: 'profile',
        component: ProfileComponent,
        canActivate: [AuthGuard],
        data: {
            allowedRoles: ['ADMIN']
        }
    },
    {
        path: 'forgotpassword',
        component: ForgotPasswordComponent
    },
    {
        path: 'reset/:token',
        component: ResetPasswordComponent
    },
    {
        path: 'addbike',
        component: AddBikeComponent,
        canActivate: [AuthGuard],
        data: {
            allowedRoles: ['ADMIN', 'USER']
        }
    },
    {
        path: 'thefts',
        component: BikeListComponent
    },
    {
        path: 'thefts/details/:id',
        component: TheftDetailsComponent,
    },
    {
        path: 'articles/lock',
        component: ArticleComponent,
        data: {
            articlePath: 'assets/articles/lockyourbike.md'
        }
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: InterceptorService,
            multi: true
        }
    ]
})
export class AppRoutingModule {}
