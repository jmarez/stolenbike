import { TestBed } from '@angular/core/testing';

import { MessagesService } from './messages.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';

describe('MessagesService', () => {
    let service: MessagesService;
    let httpMock: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({ imports: [HttpClientTestingModule] });
        service = TestBed.get(MessagesService);
        httpMock = TestBed.get(HttpTestingController);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should add theft', () => {
        const mockMessage = {
            name: 'John Doe',
            email: 'johndoe@doe.com',
            message: 'lorem ipsum'
        };
        service.sendMessage(mockMessage).subscribe();

        const req = httpMock.expectOne({
            method: 'POST',
            url: environment.apiUrl + '/messages'
        });
        expect(req.request.url).toEqual(environment.apiUrl + '/messages');
    });
});
