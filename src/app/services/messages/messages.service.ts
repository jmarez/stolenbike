import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IMessage } from 'src/app/models/message.models';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  constructor(private httpClient: HttpClient) { }

  sendMessage(message) {
      return this.httpClient.post<IMessage>(environment.apiUrl + '/messages', message);
  }
}
