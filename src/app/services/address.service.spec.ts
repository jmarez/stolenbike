import { TestBed } from '@angular/core/testing';

import { AddressService } from './address.service';
import { of } from 'rxjs';

describe('AddressService', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: AddressService = TestBed.get(AddressService);
        expect(service).toBeTruthy();
    });

    it('should return list of address', () => {
        const service: AddressService = TestBed.get(AddressService);
        const spyGetAddress = jest
            .spyOn(service.adressProvider, 'search')
            .mockReturnValue(Promise.resolve(['address1', 'address2']));
        service.getAddress('152 rue du becquerem');
        expect(spyGetAddress).toHaveBeenCalled();
    });
});
