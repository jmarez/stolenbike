import { Injectable } from '@angular/core';
import { OpenStreetMapProvider, BingProvider } from 'leaflet-geosearch';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AddressService {
    // adressProvider = new OpenStreetMapProvider({
    //     params: {
    //         limit: 30,
    //         countrycodes: 'fr'
    //     }
    // });

    adressProvider = new BingProvider({
        params: {
            key: environment.bingSecretKey
        }
    });

    constructor() {}

    getAddress(address) {
        const subject = new Subject();
        const obs = subject.asObservable();
        this.adressProvider
            .search({ query: address })
            .then(result => subject.next(result));
        return obs;
    }
}
