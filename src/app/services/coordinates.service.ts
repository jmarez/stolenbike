import { Injectable } from '@angular/core';
import { Observable, throwError, ReplaySubject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class CoordinatesService {
    constructor() {}
    take(arg0: number) {
        throw new Error('Method not implemented.');
    }

    public getPosition(): Observable<Position> {
        if (navigator.geolocation) {
            const locationSubject = new ReplaySubject<Position>(1);
            navigator.geolocation.getCurrentPosition(
                position => {
                    locationSubject.next(position);
                    locationSubject.complete();
                },
                (error: PositionError) => {
                    locationSubject.error(error);
                },
                {
                    timeout: 3000
                }
            );
            return locationSubject.asObservable();
        } else {
            return throwError('no coordinates');
        }
    }
}
