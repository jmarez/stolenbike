import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ImageService {
    constructor(private httpClient: HttpClient) {}

    getImage(filename: string): Observable<Blob> {
        return this.httpClient
            .get(environment.apiUrl + '/thefts/images/' + filename, {
                responseType: 'blob',
                observe: 'body'
            });
    }
}
