import { TestBed } from '@angular/core/testing';

import { ImageService } from './image.service';
import { HttpClient, HttpHandler } from '@angular/common/http';

describe('ImageService', () => {
    beforeEach(() =>
        TestBed.configureTestingModule({
            providers: [HttpClient, HttpHandler]
        })
    );

    it('should be created', () => {
        const service: ImageService = TestBed.get(ImageService);
        expect(service).toBeTruthy();
    });
});
