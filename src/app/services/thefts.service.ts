import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ITheft } from '../models/theft.models';

@Injectable({
    providedIn: 'root'
})
export class TheftsService {
    constructor(private http: HttpClient) {}

    addTheft(formData: FormData) {
        return this.http.post<ITheft>(environment.apiUrl + '/thefts', formData);
    }

    getThefts() {
        return this.http.get<ITheft[]>(environment.apiUrl + '/thefts', {
            observe: 'body'
        });
    }

    getTheftDetails(id: string) {
        return this.http.get<ITheft>(
            environment.apiUrl + '/thefts/details/' + id,
            { observe: 'body' }
        );
    }
}
