import { TestBed } from '@angular/core/testing';

import { TheftsService } from './thefts.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';

describe('TheftsService', () => {
    let service: TheftsService;
    let httpMock: HttpTestingController;
    beforeEach(() => {
        TestBed.configureTestingModule({ imports: [HttpClientTestingModule] });
        service = TestBed.get(TheftsService);
        httpMock = TestBed.get(HttpTestingController);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should add theft', () => {
        const theft = new FormData();
        service.addTheft(theft).subscribe();
        const req = httpMock.expectOne({ method: 'POST', url: environment.apiUrl + '/thefts' });
        expect(req.request.url).toEqual(environment.apiUrl + '/thefts');
    });

    it('Should call /thefts to get all thefts', () => {
        service.getThefts().subscribe();
        const req = httpMock.expectOne({ method: 'GET' });
        const resourceUrl = environment.apiUrl + '/thefts';
        expect(req.request.url).toEqual(resourceUrl);
    });

    it('should get theft detail by id', () => {
        const dummyId = '1234ze1234zer2134sd';
        service.getTheftDetails(dummyId).subscribe();
        const req = httpMock.expectOne({ method: 'GET' });
        const resourceUrl = environment.apiUrl + '/thefts/details/' + dummyId;
        expect(req.request.url).toEqual(resourceUrl);
    });
});
