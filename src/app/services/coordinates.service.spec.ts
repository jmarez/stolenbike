import { TestBed } from '@angular/core/testing';
import { CoordinatesService } from './coordinates.service';

describe('Service Tests', () => {
    describe('CoordinatesService', () => {
        let service: CoordinatesService;

        beforeEach(() => {
            service = TestBed.get(CoordinatesService);
        });

        describe('Service methods', () => {
            it('Should return an observable that gives the location', () => {
                // Scam typescript "readonly"
                navigator['geo' + 'location'] = {
                    getCurrentPosition: jest.fn()
                };
                jest.spyOn(navigator.geolocation, 'getCurrentPosition').mockImplementation(callBackSuccess => {
                    callBackSuccess({ coords: 'test' });
                });
                return service
                    .getPosition()
                    .toPromise()
                    .then(position => {
                        expect(navigator.geolocation.getCurrentPosition).toHaveBeenCalledTimes(1);
                        expect(position).toEqual({ coords: 'test' });
                    });
            });

            it('Should return an observable that gives the location', () => {
                // Scam typescript "readonly"
                navigator['geo' + 'location'] = {
                    getCurrentPosition: jest.fn()
                };
                jest.spyOn(navigator.geolocation, 'getCurrentPosition').mockImplementation((callBackSuccess, callbackError) => {
                    callbackError(new Error('Coordinates error'));
                });
                return service
                    .getPosition()
                    .toPromise()
                    .catch(error => {
                        expect(error.message).toEqual('Coordinates error');
                    });
            });
        });
    });
});
