import { TestBed } from '@angular/core/testing';

import { InterceptorService } from './interceptor.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AuthenticationService } from 'src/app/auth/auth.service';

describe('InterceptorService', () => {

    let service: InterceptorService;
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule]
        });
        service = TestBed.get(InterceptorService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
