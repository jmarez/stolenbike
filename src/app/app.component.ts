import { Component } from '@angular/core';
import { AuthenticationService } from './auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'biketheftsmap';

  constructor(private authService: AuthenticationService) {}

  logout() {
    this.authService.logout();
  }
}
