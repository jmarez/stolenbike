import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ProfileComponent } from './components/profile/profile.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
    NbThemeModule,
    NbLayoutModule,
    NbButtonModule,
    NbInputModule,
    NbActionsModule,
    NbCardModule,
    NbAlertModule,
    NbSelectModule,
    NbToggleModule,
    NbListModule,
    NbDatepickerModule,
    NbIconModule,
    NbDialogModule,
    NbToastrModule
} from '@nebular/theme';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { SignupComponent } from './components/signup/signup.component';
import { ConfirmationComponent } from './components/confirmation/confirmation.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { MapComponent } from './components/map/map.component';
import { LoadingComponent } from './components/loading/loading.component';
import { AddBikeComponent } from './components/add-bike/add-bike.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { CustomPopupComponent } from './components/custom-popup/custom-popup.component';
import { PictureComponent } from './components/picture/picture/picture.component';
import { TheftDetailsComponent } from './components/theft-details/theft-details.component';
import { BikeListComponent } from './components/bike-list/bike-list.component';
import { BikePictureComponent } from './components/bike-picture/bike-picture.component';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { MarkdownModule } from 'ngx-markdown';
import { ArticleComponent } from './components/articles/article/article.component';
import { FooterComponent } from './components/footer/footer.component';
import { MessageDialogComponent } from './components/message-dialog/message-dialog.component';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        ProfileComponent,
        NavbarComponent,
        HomeComponent,
        SignupComponent,
        ConfirmationComponent,
        ForgotPasswordComponent,
        ResetPasswordComponent,
        MapComponent,
        LoadingComponent,
        AddBikeComponent,
        CustomPopupComponent,
        PictureComponent,
        TheftDetailsComponent,
        BikeListComponent,
        BikePictureComponent,
        ArticleComponent,
        FooterComponent,
        MessageDialogComponent
    ],
    imports: [
        ReactiveFormsModule,
        HttpClientModule,
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        NbThemeModule.forRoot({ name: 'default' }),
        NbLayoutModule,
        NbButtonModule,
        NbInputModule,
        NbActionsModule,
        NbCardModule,
        NbAlertModule,
        NbSelectModule,
        NbToggleModule,
        NbListModule,
        NbDatepickerModule.forRoot(),
        NgSelectModule,
        NbEvaIconsModule,
        NbIconModule,
        MarkdownModule.forRoot(),
        NbDialogModule.forRoot(),
        NbToastrModule.forRoot()
    ],
    bootstrap: [AppComponent],
    entryComponents: [CustomPopupComponent, MessageDialogComponent],
})
export class AppModule {}
