export const environment = {
  production: true,
  apiUrl: '/api',
  bingSecretKey: '%BING_SECRET_KEY%'
};
