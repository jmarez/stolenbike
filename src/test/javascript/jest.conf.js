module.exports = {
  preset: 'jest-preset-angular',
  setupFilesAfterEnv: ['<rootDir>/src/setup-jest.ts'],
  coverageDirectory: '<rootDir>/build/test-results/',
  globals: {
      'ts-jest': {
          tsConfig: 'tsconfig.json',
          stringifyContentPathRegex: '\\.html$',
          diagnostics: false,
          astTransformers: [
            require.resolve('jest-preset-angular/build/StripStylesTransformer'),
            require.resolve('jest-preset-angular/build/InlineFilesTransformer')
          ]
      }
  },
  coveragePathIgnorePatterns: ['<rootDir>/src/test/javascript'],
  moduleNameMapper: {
      'app/(.*)': '<rootDir>/src/app/$1'
  },
  reporters: ['default', ['jest-junit', { output: './build/test-results/TESTS-results-jest.xml' }]],
  testResultsProcessor: 'jest-sonar-reporter',
  transformIgnorePatterns: ['node_modules/(?!@angular/common/locales)', '<rootDir>/node_modules/(?!ngx-auto-unsubscribe/)'],
  testMatch: ['<rootDir>/src/app/**/+(*.)+(spec.ts)'],
  testPathIgnorePatterns: ['<rootDir>/src/test/*'],
  rootDir: '../../../',
  testURL: 'http://localhost/'
};
